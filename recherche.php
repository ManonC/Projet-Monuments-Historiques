<!doctype html>
<html lang="fr">
  <head>
  <meta charset="utf-8" />

  <link rel = "stylesheet" href = "recherche.css">
  </head>

  <body>

    <?php
      include 'bandeau.html';
    ?>


  </br>
      <div>
        <p>
          <br/>
          <h1> Rechercher un monument </h1>
          <br/>
        </p>
      </div>

    <form action="resultatRecherche.php" method="post">

    </br>
      <table id='recherche_Monument'>
      <tr>
        <td><input type = "radio" name = "recherche" value ="region" onclick="RechercheTitre()" /> Région</td>
        <td><input type="text" id="region" name="region" placeholder="nom de la région"/></td>

      </tr>

        <tr>
          <td><input type = "radio" name = "recherche" value ="Dpt" onclick="RechercheDpt()"/> Département</td>
          <td><input type="text" id="Dpt" name="Dpt" placeholder="nom du département"/></td>

        </tr>


        <tr>
          <td><input type = "radio" name = "recherche" value ="ville" onclick="RechercheActeur()"/> Commune</td>
          <td><input type="text" id="ville" name="ville" placeholder="nom de la commune"/></td>

        </tr>

        <tr>
          <td><input type = "radio" name = "recherche" value ="objet" onclick="RechercheCouleur()" /> Monument</td>
          <td><input type="text" id="objet" name="objet" placeholder="nom du monument"/></td>
        </tr>
      </table>
    </br></br>
      <input type="submit" id = "bouton" value="Rechercher"></input>
    </br></br></br>
    </form>

  </body>
</html>

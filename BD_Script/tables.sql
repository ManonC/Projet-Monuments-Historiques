DROP TABLE OBJET;
DROP TABLE COMMUNE;
DROP TABLE DEPARTEMENT;
DROP TABLE REGION;



CREATE TABLE REGION(
  nomRegion VARCHAR (30)
);


CREATE TABLE DEPARTEMENT(
  numeroDept INT (3),
  nomDpt VARCHAR (30),
  nomRegion VARCHAR (30)
);



CREATE TABLE COMMUNE(
  nomCommune VARCHAR (40),
  numeroDept INT(3)
);

CREATE TABLE OBJET(
  refMonument INT (8) primary key auto_increment,
  nomEdifice VARCHAR (50),
  nomObjet VARCHAR (30),
  descriptionObjet VARCHAR (500),
  materiaux VARCHAR (100),
  auteur VARCHAR (40),
  siecle VARCHAR (40),
  dateAjout VARCHAR(10),
  status VARCHAR(40),
  nomImage VARCHAR(30),
  nomCommune VARCHAR (40)
);

ALTER TABLE COMMUNE
ADD PRIMARY KEY(nomCommune);

ALTER TABLE DEPARTEMENT
ADD PRIMARY KEY(nomDpt);

ALTER TABLE REGION
ADD PRIMARY KEY(nomRegion);

/*ALTER TABLE OBJET
ADD PRIMARY KEY(refMonument);*/

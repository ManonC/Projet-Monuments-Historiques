<!doctype html>
<html lang="fr">
  <head>
  <meta charset="utf-8" />

  <link rel = "stylesheet" href = "ajout.css">
  </head>

  <body>

    <form name="f1" method="GET" action="inserer.php" enctype="multipart/form-data">
    <?php
      include 'bandeau.html';
      require "Connexion.php";
      $connexion=ConnexionMySQL();
    ?>

  </br>
      <div>
        <p>
          <br/>
          <h1> Ajouter un monument </h1>
          <br/>
        </p>
      </div>

    <div id="global">
      <div id ="div1">


        <p> Nom de l'objet : <input required type="text"
                                      name="nomObjet"
                                      maxlength="30"
                                      pattern="[A-Za-zùéçêôëö '-]{1,30}"
                                      title="Entrez un nom de monument correct (pas de chiffres ni de caractères spéciaux)."/> </p>



        <p> Région : <input required type="text"
                                      name="nomRegion"
                                      maxlength="30"
                                      pattern="[A-Za-zùéçêôëö '-]{1,30}"
                                      title="Entrez un nom de région correct (pas de chiffres ni de caractères spéciaux)"/> </p>

        <p> Nom du département : <input required type="text"
                                      name="nomDpt"
                                      maxlength="30"
                                      pattern="[A-Za-zùéçêôëö '-]{1,30}"
                                      title="Entrez un nom de département correct (pas de chiffres ni de caractères spéciaux)"/> </p>

        <p> Numéro du département : <input required type="text"
                                           name="numeroDept"
                                           maxlength="3"
                                           pattern="[0-9]{1,3}"
                                           title="Entrez un nombre de 1 à 3 chiffres."/> </p>

         <p> Nom de la commune : <input required type="text"
                                            name="nomCommune"
                                            maxlength="40"
                                            pattern="[A-Za-zùéçêôëö '-]{1,40}"
                                            title="Entrez un nom de commune correct (pas de chiffres ni de caractères spéciaux)"/> </p>

        <p> Nom de l'édifice </br>où se trouve l'objet : <input type="text"
                                  name="nomEdifice"
                                  maxlength="30"
                                  pattern="[A-Za-zùéçêôëö '-]{1,30}"
                                  title="Entrez un nom de localisation correct (pas de chiffres ni de caractères spéciaux)"/> </p>

        <p> Auteur : <input required type="text"
                            name="auteur"
                            maxlength="40"
                            pattern="[A-Za-zùéçêôëö '-]{1,40}"
                            title="Entrez un nom d'auteur correct (pas de chiffres ni de caractères spéciaux)"/>
                            
        <p> Description : </p>
        <p> <textarea type="text"
                      name="descriptionObjet"
                      rows="10"
                      cols="50"
                      maxlength="500"
                      title="500 caractères maximum" > </textarea>
        </p>

      </div>
      <div id ="div2">

        <p> Matériaux : <input required type="text"
                            name="materiaux"
                            maxlength="100"
                            title="100 caractères maximum"/> </p>


        <p> Siècle : <input required type="text"
                            name="siecle"
                            maxlength="40"
                            title="40 caractères maximum"/> </p>


        <p> Date ajout au registre : <input required type="text"
                                            name="dateAjout"
                                            maxlength="10"
                                            size = "10"
                                            pattern="(0[1-9]|1[012])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d"
                                            placeholder="jj/mm/aaaa"
                                            title="10 caractères sous la forme : jj/mm/aaaa"/> </p>


        <p>Statut :
        <ul>
          <li>Propriété de la commune<input type="radio" name="status" value="commune"></li><br/>
    		  <li>Propriété publique<input type="radio" name="status" value="publique"></li>
        </ul>
        </p> </br>



        <p> Image : </p>
        <input type="file"
               name="image"
               accept="image/*" >


        <div>
          <p>
            <br/>
            <br/>
          </p>
        </div>

        <input type="submit" value ="Ajouter" id= "bouton">

      </div>
    </div>

  </body>
</html>
